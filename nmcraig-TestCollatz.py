#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, cycle_length, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----
        
    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "1000 35000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1000)
        self.assertEqual(j, 35000)

    def test_rea_3(self):
        s = "100000 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100000)
        self.assertEqual(j, 1)
    # ----
    # eval
    # ----

    def test_cycle_length_1(self):
        v = cycle_length(100)
        self.assertEqual(v, 26)

    def test_cycle_length_2(self):
        v = cycle_length(999999)
        self.assertEqual(v, 259)

    def test_cycle_length_2(self):
        v = cycle_length(777234)
        self.assertEqual(v, 119)


        
    #need to fix tests to have correct values
    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)
        
    def test_eval_4(self):
        v = collatz_eval(10000, 1)
        self.assertEqual(v, 262)

    #New Unit Tests
    
    # -----
    # print
    # -----
      
    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1000, 9, 200012)
        self.assertEqual(w.getvalue(), "1000 9 200012\n")

    def test_print(self):
        w = StringIO()
        collatz_print(w, 999999, 109123, 203478)
        self.assertEqual(w.getvalue(), "999999 109123 203478\n")

    # -----
    # solve
    # -----

#Fix values for max cycle length of w.getvalue
    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
       
'''
    def test_solve(self):
        r = StringIO("1 1000\n100 20000\n201 21\n9000 100000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1000 179\n100 20000 279\n201 21 125\n9000 100000 351\n")
'''
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
